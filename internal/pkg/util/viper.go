package util

import (
	"sync"

	"github.com/spf13/viper"
)

var (
	once = new(sync.Once)
	vip  *viper.Viper
)

func GetViper() *viper.Viper {
	once.Do(func() {
		if vip == nil {
			vip = viper.New()
		}
	})
	return vip
}
