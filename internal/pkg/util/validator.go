package util

import (
	"errors"
	"reflect"
	"strings"

	translator "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
)

func ValidateIsNil(object interface{}) bool {
	// 判断动态类型是否为nil [object=nil]
	if reflect.DeepEqual(object, nil) {
		return true
	}
	// 判断动态值是否为nil [object=(*int)(nil)]
	value := reflect.ValueOf(object)
	if value.Kind() == reflect.Ptr {
		return value.IsNil()
	}
	return false
}

func ValidateStruct(object interface{}, language string, translation bool) error {
	if ValidateIsNil(object) {
		return errors.New("validation objects are not allowed to be empty")
	}
	validate := validator.New()
	language = strings.TrimSpace(language)
	var translations translator.Translator
	if translation && len(language) > 0 {
		translations = universalTranslator(validate, language)
	}
	errs := validate.Struct(object)
	if errs != nil {
		for _, err := range errs.(validator.ValidationErrors) {
			if translation && len(language) > 0 && !ValidateIsNil(translations) {
				return errors.New(err.Translate(translations))
			} else {
				return err
			}
		}
	}
	return nil
}
