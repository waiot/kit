package log

import (
	"io"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

type Logrus struct {
	Level string `mapstructure:"level" ini:"level" json:"level" toml:"level" xml:"level" yaml:"level" validate:"required,oneof=trace debug info warn error fatal panic"` // 日志级别: [trace>debug>info(默认)>warn>error>fatal>panic]
}

func (log *Logger) InitLogrus() {
	logrus.SetReportCaller(true)
	level := strings.TrimSpace(log.Logrus.Level)
	switch {
	case strings.EqualFold(level, "trace"):
		logrus.SetLevel(logrus.TraceLevel)
	case strings.EqualFold(level, "debug"):
		logrus.SetLevel(logrus.DebugLevel)
	case strings.EqualFold(level, "info"):
		logrus.SetLevel(logrus.InfoLevel)
	case strings.EqualFold(level, "warn"):
		logrus.SetLevel(logrus.WarnLevel)
	case strings.EqualFold(level, "error"):
		logrus.SetLevel(logrus.ErrorLevel)
	case strings.EqualFold(level, "fatal"):
		logrus.SetLevel(logrus.FatalLevel)
	case strings.EqualFold(level, "panic"):
		logrus.SetLevel(logrus.PanicLevel)
	}
	callerPrettyfier := func(frame *runtime.Frame) (function string, file string) {
		return strings.Join([]string{frame.Func.Name(), strconv.Itoa(frame.Line)}, ":"), filepath.Base(frame.File)
	}
	log.Format = strings.TrimSpace(log.Format)
	switch {
	case strings.EqualFold(log.Format, "text"):
		textFormatter := &logrus.TextFormatter{
			FullTimestamp:    true,
			ForceQuote:       true,
			TimestampFormat:  time.RFC3339Nano,
			CallerPrettyfier: callerPrettyfier,
		}
		if log.Develop {
			if log.Output != nil {
				if log.Output.Console {
					textFormatter.ForceColors = true
				}
			}
		}
		logrus.SetFormatter(textFormatter)
	case strings.EqualFold(log.Format, "json"):
		jsonFormatter := &logrus.JSONFormatter{
			TimestampFormat:  time.RFC3339Nano,
			CallerPrettyfier: callerPrettyfier,
		}
		// if log.Develop {
		// 	if log.Output != nil {
		// 		if log.Output.Console {
		// 			jsonFormatter.PrettyPrint = true
		// 		}
		// 	}
		// }
		logrus.SetFormatter(jsonFormatter)
	}
	logrus.SetOutput(io.MultiWriter(os.Stdout))
}

// develop&& console -- trace log
// develop && console -- color

/*
Panic：记录日志，然后panic。
Fatal：致命错误，出现错误时程序无法正常运转。输出日志后，程序退出；
Error：错误日志，需要查看原因；
Warn：警告信息，提醒程序员注意；
Info：关键操作，核心流程的日志；
Debug：一般程序中输出的调试信息；
Trace：很细粒度的信息，一般用不到；
*/
