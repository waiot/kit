package log

// 日志输出到文件
type File struct {
	Path   string  `mapstructure:"path" ini:"path" json:"path" toml:"path" xml:"path" yaml:"path" validate:"required,file"`                 // 日志文件存储位置
	Prefix string  `mapstructure:"prefix" ini:"prefix" json:"prefix" toml:"prefix" xml:"prefix" yaml:"prefix" validate:"required"`          // 日志文件前缀
	Suffix *Suffix `mapstructure:"suffix" ini:"log.file.suffix" json:"suffix" toml:"suffix" xml:"suffix" yaml:"suffix" validate:"required"` // 日志文件后缀
	Max    *Max    `mapstructure:"max" ini:"log.file.max" json:"max" toml:"max" xml:"max" yaml:"max" validate:"required"`                   // 日志文件最大参数
}

// 日志文件后缀
type Suffix struct {
	Trace  string `mapstructure:"trace" ini:"trace" json:"trace" toml:"trace" xml:"trace" yaml:"trace" validate:"required"`       // trace 日志文件后缀
	Debug  string `mapstructure:"debug" ini:"debug" json:"debug" toml:"debug" xml:"debug" yaml:"debug" validate:"required"`       // debug 日志文件后缀
	Info   string `mapstructure:"info" ini:"info" json:"info" toml:"info" xml:"info" yaml:"info" validate:"required"`             // info 日志文件后缀
	Warn   string `mapstructure:"warn" ini:"warn" json:"warn" toml:"warn" xml:"warn" yaml:"warn" validate:"required"`             // warn 日志文件后缀
	Error  string `mapstructure:"error" ini:"error" json:"error" toml:"error" xml:"error" yaml:"error" validate:"required"`       // error 日志文件后缀
	Dpanic string `mapstructure:"dpanic" ini:"dpanic" json:"dpanic" toml:"dpanic" xml:"dpanic" yaml:"dpanic" validate:"required"` // dpanic 日志文件后缀
	Panic  string `mapstructure:"panic" ini:"panic" json:"panic" toml:"panic" xml:"panic" yaml:"panic" validate:"required"`       // panic 日志文件后缀
	Fatal  string `mapstructure:"fatal" ini:"fatal" json:"fatal" toml:"fatal" xml:"fatal" yaml:"fatal" validate:"required"`       // fatal 日志文件后缀
}

// 日志文件最大参数
type Max struct {
	Size    int `mapstructure:"size" ini:"size" json:"size" toml:"size" xml:"size" yaml:"size" validate:"required"`                   // 日志文件大小(M)
	Backups int `mapstructure:"backups" ini:"backups" json:"backups" toml:"backups" xml:"backups" yaml:"backups" validate:"required"` // 最多存在多少个切片文件
	Age     int `mapstructure:"age" ini:"age" json:"age" toml:"age" xml:"age" yaml:"age" validate:"required"`                         // 保存的最大天数
}
