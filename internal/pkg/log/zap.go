package log

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/natefinch/lumberjack"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// 日志模型
type Zap struct {
	Level     string     `mapstructure:"level" ini:"level" json:"level" toml:"level" xml:"level" yaml:"level" validate:"required,oneof=debug info warn error dpanic panic fatal"` // 日志级别: ["debug"(默认),"info","warn","error","dpanic","panic","fatal"]
	zapConfig zap.Config `mapstructure:"-" ini:"-" json:"-" toml:"-" xml:"-" yaml:"-" validate:"-"`                                                                               // zap配置
}

// 编码器配置
func (logger *Logger) encoderConfig() zapcore.EncoderConfig {
	enc := zapcore.EncoderConfig{
		TimeKey:          "time",                         // time 键
		LevelKey:         "level",                        // level 键
		NameKey:          "namespace",                    // namespace 键
		CallerKey:        "caller",                       // caller 键
		FunctionKey:      zapcore.OmitKey,                // function 键
		MessageKey:       "message",                      // message 键
		StacktraceKey:    "stack",                        // stacktrace 键
		LineEnding:       zapcore.DefaultLineEnding,      // 换行,默认为\n
		ConsoleSeparator: " ",                            // 日志一行内多项分隔,默认为\t
		EncodeLevel:      zapcore.CapitalLevelEncoder,    // 日志级别无颜色显示
		EncodeTime:       zapcore.RFC3339NanoTimeEncoder, // 时间编码格式
		EncodeDuration:   zapcore.NanosDurationEncoder,   // 时间纳秒编码
		EncodeCaller:     zapcore.ShortCallerEncoder,     // 调用方显示简短格式
	}
	if logger.Develop {
		enc.EncodeLevel = zapcore.CapitalColorLevelEncoder // 日志级别区分颜色显示
		enc.ConsoleSeparator = "\t"                        // 日志一行内多项分隔,默认为\t
	}
	return enc
}

// 创建编码器
func (logger *Logger) encoder(enc zapcore.EncoderConfig) zapcore.Encoder {
	var encoder zapcore.Encoder
	if strings.EqualFold("json", logger.Format) {
		enc.EncodeLevel = zapcore.CapitalLevelEncoder // 日志级别无颜色显示
		encoder = zapcore.NewJSONEncoder(enc)         // json格式编码器
	} else {
		encoder = zapcore.NewConsoleEncoder(enc) // console格式编码器
	}
	return encoder
}

// 日志级别输出
func (logger *Logger) levelOutput(encoder zapcore.Encoder) []zapcore.Core {
	zlog := logger.Zap
	if strings.EqualFold("text", logger.Format) {
		logger.Format = "console"
	}
	if logger.Develop {
		zlog.zapConfig = zap.Config{
			Level:             zap.NewAtomicLevelAt(zap.DebugLevel),
			Development:       logger.Develop,
			DisableCaller:     false,
			DisableStacktrace: false,
			Sampling: &zap.SamplingConfig{
				Initial:    100,
				Thereafter: 100,
			},
			Encoding:         logger.Format,
			EncoderConfig:    logger.encoderConfig(),
			OutputPaths:      []string{"stdout", "stderr"},
			ErrorOutputPaths: []string{"stderr"},
		}

	} else {
		zlog.zapConfig = zap.Config{
			Level:             zap.NewAtomicLevelAt(zap.InfoLevel),
			Development:       logger.Develop,
			DisableCaller:     false,
			DisableStacktrace: false,
			Sampling: &zap.SamplingConfig{
				Initial:    100,
				Thereafter: 100,
			},
			Encoding:         logger.Format,
			EncoderConfig:    logger.encoderConfig(),
			OutputPaths:      []string{"stderr"},
			ErrorOutputPaths: []string{"stderr"},
		}

	}
	level := strings.TrimSpace(zlog.Level)
	switch {
	case strings.EqualFold(level, "debug"), strings.EqualFold(level, "trace"):
		zlog.zapConfig.Level = zap.NewAtomicLevelAt(zap.DebugLevel)
	case strings.EqualFold(level, "info"):
		zlog.zapConfig.Level = zap.NewAtomicLevelAt(zap.InfoLevel)
	case strings.EqualFold(level, "warn"):
		zlog.zapConfig.Level = zap.NewAtomicLevelAt(zap.WarnLevel)
	case strings.EqualFold(level, "error"):
		zlog.zapConfig.Level = zap.NewAtomicLevelAt(zap.ErrorLevel)
	case strings.EqualFold(level, "dpanic"):
		zlog.zapConfig.Level = zap.NewAtomicLevelAt(zap.DPanicLevel)
	case strings.EqualFold(level, "panic"):
		zlog.zapConfig.Level = zap.NewAtomicLevelAt(zap.PanicLevel)
	case strings.EqualFold(level, "fatal"):
		zlog.zapConfig.Level = zap.NewAtomicLevelAt(zap.FatalLevel)
	default:
		zlog.zapConfig.Level = zap.NewAtomicLevelAt(zap.DebugLevel)
	}

	// 日志级别
	debugPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool { // debug 级别
		return lvl == zapcore.DebugLevel && zapcore.DebugLevel-zlog.zapConfig.Level.Level() > -1
	})
	infoPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool { // info 级别
		return lvl == zapcore.InfoLevel && zapcore.InfoLevel-zlog.zapConfig.Level.Level() > -1
	})
	warnPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool { // warn 级别
		return lvl == zapcore.WarnLevel && zapcore.WarnLevel-zlog.zapConfig.Level.Level() > -1
	})
	errorPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool { // error 级别
		return lvl == zapcore.ErrorLevel && zapcore.ErrorLevel-zlog.zapConfig.Level.Level() > -1
	})
	dpanicPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool { // dpanic 级别
		return lvl == zapcore.DPanicLevel && zapcore.DPanicLevel-zlog.zapConfig.Level.Level() > -1
	})
	panicPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool { // panic 级别
		return lvl == zapcore.PanicLevel && zapcore.PanicLevel-zlog.zapConfig.Level.Level() > -1
	})
	fatalPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool { // fatal 级别
		return lvl == zapcore.FatalLevel && zapcore.FatalLevel-zlog.zapConfig.Level.Level() > -1
	})
	// 输出
	var (
		debugCore  zapcore.Core
		infoCore   zapcore.Core
		warnCore   zapcore.Core
		errorCore  zapcore.Core
		dpanicCore zapcore.Core
		panicCore  zapcore.Core
		fatalCore  zapcore.Core
	)
	// zlog.handleOutPut()
	if logger.Output.Console {
		debugCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), debugPriority)   // 第三个及之后的参数为日志级别
		infoCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), infoPriority)     // 第三个及之后的参数为日志级别
		warnCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), warnPriority)     // 第三个及之后的参数为日志级别
		errorCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), errorPriority)   // 第三个及之后的参数为日志级别
		dpanicCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), dpanicPriority) // 第三个及之后的参数为日志级别
		panicCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), panicPriority)   // 第三个及之后的参数为日志级别
		fatalCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), fatalPriority)   // 第三个及之后的参数为日志级别
	}
	if logger.Output.File {
		path := strings.TrimSpace(logger.File.Path)
		// debug 文件 write sync
		debugWS := zapcore.AddSync(&lumberjack.Logger{
			Filename:   filepath.Join(path, fmt.Sprintf("%s-%s", logger.File.Prefix, logger.File.Suffix.Debug)), // 日志文件存放目录，如果文件夹不存在自动创建
			MaxSize:    logger.File.Max.Size,                                                                    // 日志文件大小(M)
			MaxBackups: logger.File.Max.Backups,                                                                 // 最多存在多少个切片文件
			MaxAge:     logger.File.Max.Age,                                                                     // 保存的最大天数
			LocalTime:  true,                                                                                    // 日志是否使用本地时间，默认使用UTC时间
			Compress:   true,                                                                                    // 是否进行gzip压缩，默认未压缩
		})
		// info 文件 write sync
		infoWS := zapcore.AddSync(&lumberjack.Logger{
			Filename:   filepath.Join(path, fmt.Sprintf("%s-%s", logger.File.Prefix, logger.File.Suffix.Info)), // 日志文件存放目录，如果文件夹不存在自动创建
			MaxSize:    logger.File.Max.Size,                                                                   // 日志文件大小(M)
			MaxBackups: logger.File.Max.Backups,                                                                // 最多存在多少个切片文件
			MaxAge:     logger.File.Max.Age,                                                                    // 保存的最大天数
			LocalTime:  true,                                                                                   // 日志是否使用本地时间，默认使用UTC时间
			Compress:   true,                                                                                   // 是否进行gzip压缩，默认未压缩
		})
		// warn 文件 write sync
		warnWS := zapcore.AddSync(&lumberjack.Logger{
			Filename:   filepath.Join(path, fmt.Sprintf("%s-%s", logger.File.Prefix, logger.File.Suffix.Warn)), // 日志文件存放目录，如果文件夹不存在自动创建
			MaxSize:    logger.File.Max.Size,                                                                   // 日志文件大小(M)
			MaxBackups: logger.File.Max.Backups,                                                                // 最多存在多少个切片文件
			MaxAge:     logger.File.Max.Age,                                                                    // 保存的最大天数
			LocalTime:  true,                                                                                   // 日志是否使用本地时间，默认使用UTC时间
			Compress:   true,                                                                                   // 是否进行gzip压缩，默认未压缩
		})
		// error 文件 write sync
		errorWS := zapcore.AddSync(&lumberjack.Logger{
			Filename:   filepath.Join(path, fmt.Sprintf("%s-%s", logger.File.Prefix, logger.File.Suffix.Error)), // 日志文件存放目录，如果文件夹不存在自动创建
			MaxSize:    logger.File.Max.Size,                                                                    // 日志文件大小(M)
			MaxBackups: logger.File.Max.Backups,                                                                 // 最多存在多少个切片文件
			MaxAge:     logger.File.Max.Age,                                                                     // 保存的最大天数
			LocalTime:  true,                                                                                    // 日志是否使用本地时间，默认使用UTC时间
			Compress:   true,                                                                                    // 是否进行gzip压缩，默认未压缩
		})
		// dpanic 文件 write sync
		dpanicWS := zapcore.AddSync(&lumberjack.Logger{
			Filename:   filepath.Join(path, fmt.Sprintf("%s-%s", logger.File.Prefix, logger.File.Suffix.Dpanic)), // 日志文件存放目录，如果文件夹不存在自动创建
			MaxSize:    logger.File.Max.Size,                                                                     // 日志文件大小(M)
			MaxBackups: logger.File.Max.Backups,                                                                  // 最多存在多少个切片文件
			MaxAge:     logger.File.Max.Age,                                                                      // 保存的最大天数
			LocalTime:  true,                                                                                     // 日志是否使用本地时间，默认使用UTC时间
			Compress:   true,                                                                                     // 是否进行gzip压缩，默认未压缩
		})
		// panic 文件 write sync
		panicWS := zapcore.AddSync(&lumberjack.Logger{
			Filename:   filepath.Join(path, fmt.Sprintf("%s-%s", logger.File.Prefix, logger.File.Suffix.Panic)), // 日志文件存放目录，如果文件夹不存在自动创建
			MaxSize:    logger.File.Max.Size,                                                                    // 日志文件大小(M)
			MaxBackups: logger.File.Max.Backups,                                                                 // 最多存在多少个切片文件
			MaxAge:     logger.File.Max.Age,                                                                     // 保存的最大天数
			LocalTime:  true,                                                                                    // 日志是否使用本地时间，默认使用UTC时间
			Compress:   true,                                                                                    // 是否进行gzip压缩，默认未压缩
		})
		// fatal 文件 write sync
		fatalWS := zapcore.AddSync(&lumberjack.Logger{
			Filename:   filepath.Join(path, fmt.Sprintf("%s-%s", logger.File.Prefix, logger.File.Suffix.Fatal)), // 日志文件存放目录，如果文件夹不存在自动创建
			MaxSize:    logger.File.Max.Size,                                                                    // 日志文件大小(M)
			MaxBackups: logger.File.Max.Backups,                                                                 // 最多存在多少个切片文件
			MaxAge:     logger.File.Max.Age,                                                                     // 保存的最大天数
			LocalTime:  true,                                                                                    // 日志是否使用本地时间，默认使用UTC时间
			Compress:   true,                                                                                    // 是否进行gzip压缩，默认未压缩
		})
		if len(path) == 0 && logger.Output.Console {
			debugCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), debugPriority)   // 第三个及之后的参数为日志级别
			infoCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), infoPriority)     // 第三个及之后的参数为日志级别
			warnCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), warnPriority)     // 第三个及之后的参数为日志级别
			errorCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), errorPriority)   // 第三个及之后的参数为日志级别
			dpanicCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), dpanicPriority) // 第三个及之后的参数为日志级别
			panicCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), panicPriority)   // 第三个及之后的参数为日志级别
			fatalCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), fatalPriority)   // 第三个及之后的参数为日志级别
		} else {
			debugCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(debugWS, zapcore.AddSync(os.Stdout)), debugPriority)    // 第三个及之后的参数为日志级别
			infoCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(infoWS, zapcore.AddSync(os.Stdout)), infoPriority)       // 第三个及之后的参数为日志级别
			warnCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(warnWS, zapcore.AddSync(os.Stdout)), warnPriority)       // 第三个及之后的参数为日志级别
			errorCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(errorWS, zapcore.AddSync(os.Stdout)), errorPriority)    // 第三个及之后的参数为日志级别
			dpanicCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(dpanicWS, zapcore.AddSync(os.Stdout)), dpanicPriority) // 第三个及之后的参数为日志级别
			panicCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(panicWS, zapcore.AddSync(os.Stdout)), panicPriority)    // 第三个及之后的参数为日志级别
			fatalCore = zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(fatalWS, zapcore.AddSync(os.Stdout)), fatalPriority)    // 第三个及之后的参数为日志级别
		}
	}
	var cores []zapcore.Core
	cores = append(cores, debugCore, infoCore, warnCore, errorCore, dpanicCore, panicCore, fatalCore)
	return cores
}
func (logger *Logger) InitZap() {
	enc := logger.encoderConfig()
	encoder := logger.encoder(enc)
	cores := logger.levelOutput(encoder)
	zapLog := zap.New(zapcore.NewTee(cores...), zap.AddCaller()).Named(logger.Namespace) // zap.AddCaller() 显示文件名和行号
	defer zapLog.Sync()
	zap.ReplaceGlobals(zapLog) // 全局替换生效
}
