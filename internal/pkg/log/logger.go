package log

import "gitee.com/waiot/kit/internal/pkg/util"

type Logger struct {
	Record    string  `mapstructure:"record" ini:"record" json:"record" toml:"record" xml:"record" yaml:"record" validate:"required,oneof=log logrus zap"` // 日志记录器
	Develop   bool    `mapstructure:"develop" ini:"develop" json:"develop" toml:"develop" xml:"develop" yaml:"develop" validate:"required"`                // 是否是开发模式
	Namespace string  `mapstructure:"namespace" ini:"namespace" json:"namespace" toml:"namespace" xml:"namespace" yaml:"namespace" validate:"required"`    // 日志命名空间
	Format    string  `mapstructure:"format" ini:"format" json:"format" toml:"format" xml:"format" yaml:"format" validate:"required,oneof=text json"`      // 日志格式: ["text"(默认),"json"]
	Output    *OutPut `mapstructure:"output" ini:"log.output" json:"output" toml:"output" xml:"output" yaml:"output" validate:"required"`                  // 日志输出类型
	Logrus    *Logrus `mapstructure:"logrus" ini:"log.logrus" json:"logrus" toml:"logrus" xml:"logrus" yaml:"logrus" validate:"-"`                         // zap日志配置信息
	Zap       *Zap    `mapstructure:"zap" ini:"log.zap" json:"zap" toml:"zap" xml:"zap" yaml:"zap" validate:"-"`                                           // zap日志配置信息
	File      *File   `mapstructure:"file" ini:"log.output.file" json:"file" toml:"file" xml:"file" yaml:"file" validate:"-"`                              // 输出到 文件
	// Log       *Log    `mapstructure:"log" ini:"log.log" json:"log" toml:"log" xml:"log" yaml:"log" validate:"-"`                                           // zap日志配置信息
}

// 日志输出
type OutPut struct {
	Console  bool `mapstructure:"console" ini:"console" json:"console" toml:"console" xml:"console" yaml:"console" validate:"-"`       // 输出到 控制台
	File     bool `mapstructure:"file" ini:"file" json:"file" toml:"file" xml:"file" yaml:"file" validate:"-"`                         // 输出到 文件
	Kafka    bool `mapstructure:"kafka" ini:"kafka" json:"kafka" toml:"kafka" xml:"kafka" yaml:"kafka" validate:"-"`                   // 输出到 kafka
	Redis    bool `mapstructure:"redis" ini:"redis" json:"redis" toml:"redis" xml:"redis" yaml:"redis" validate:"-"`                   // 输出到 redis
	Influxdb bool `mapstructure:"influxdb" ini:"influxdb" json:"influxdb" toml:"influxdb" xml:"influxdb" yaml:"influxdb" validate:"-"` // 输出到 influxdb
	Mysql    bool `mapstructure:"mysql" ini:"mysql" json:"mysql" toml:"mysql" xml:"mysql" yaml:"mysql" validate:"-"`                   // 输出到 mysql
}

// 默认参数设定
func (logger *Logger) DefaultSetting() {
	viper := util.GetViper()
	viper.SetDefault("log.record", "logrus")
	viper.SetDefault("log.develop", true)
	viper.SetDefault("log.namespace", "app")
	viper.SetDefault("log.format", "text")
	viper.SetDefault("log.output.console", true)
	viper.SetDefault("log.output.file", false)
	viper.SetDefault("log.output.kafka", false)
	viper.SetDefault("log.output.redis", false)
	viper.SetDefault("log.output.influxdb", false)
	viper.SetDefault("log.output.mysql", false)
	viper.SetDefault("log.logrus.level", "trace")
	viper.SetDefault("log.zap.level", "debug")
	viper.SetDefault("log.file.path", "./logs")
	viper.SetDefault("log.file.prefix", "app")
	viper.SetDefault("log.file.suffix.trace", "trace")
	viper.SetDefault("log.file.suffix.debug", "debug")
	viper.SetDefault("log.file.suffix.info", "info")
	viper.SetDefault("log.file.suffix.warn", "warn")
	viper.SetDefault("log.file.suffix.error", "error")
	viper.SetDefault("log.file.suffix.dpanic", "dpanic")
	viper.SetDefault("log.file.suffix.panic", "panic")
	viper.SetDefault("log.file.suffix.fatal", "fatal")
	viper.SetDefault("log.file.max.size", 10)
	viper.SetDefault("log.file.max.backups", 30)
	viper.SetDefault("log.file.max.age", 30)
	viper.AutomaticEnv()
	viper.SetDefault("LOG_RECORD", "logrus")
	viper.SetDefault("LOG_DEVELOP", true)
	viper.SetDefault("LOG_NAMESPACE", "app")
	viper.SetDefault("LOG_FORMAT", "text")
	viper.SetDefault("LOG_OUTPUT_CONSOLE", true)
	viper.SetDefault("LOG_OUTPUT_FILE", false)
	viper.SetDefault("LOG_OUTPUT_KAFKA", false)
	viper.SetDefault("LOG_OUTPUT_REDIS", false)
	viper.SetDefault("LOG_OUTPUT_INFLUXDB", false)
	viper.SetDefault("LOG_OUTPUT_MYSQL", false)
	viper.SetDefault("LOG_LOGRUS_LEVEL", "trace")
	viper.SetDefault("LOG_ZAP_LEVEL", "debug")
	viper.SetDefault("LOG_FILE_PATH", "./logs")
	viper.SetDefault("LOG_FILE_PREFIX", "app")
	viper.SetDefault("LOG_FILE_SUFFIX_TRACE", "trace")
	viper.SetDefault("LOG_FILE_SUFFIX_DEBUG", "debug")
	viper.SetDefault("LOG_FILE_SUFFIX_INFO", "info")
	viper.SetDefault("LOG_FILE_SUFFIX_WARN", "warn")
	viper.SetDefault("LOG_FILE_SUFFIX_ERROR", "error")
	viper.SetDefault("LOG_FILE_SUFFIX_DPANIC", "dpanic")
	viper.SetDefault("LOG_FILE_SUFFIX_PANIC", "panic")
	viper.SetDefault("LOG_FILE_SUFFIX_FATAL", "fatal")
	viper.SetDefault("LOG_FILE_MAX_SIZE", 10)
	viper.SetDefault("LOG_FILE_MAX_BACKUPS", 30)
	viper.SetDefault("LOG_FILE_MAX_AGE", 30)
}
