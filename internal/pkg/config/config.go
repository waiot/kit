package config

import (
	"gitee.com/waiot/kit/internal/pkg/influxdb"
	"gitee.com/waiot/kit/internal/pkg/kafka"
	"gitee.com/waiot/kit/internal/pkg/log"
	"gitee.com/waiot/kit/internal/pkg/mysql"
	"gitee.com/waiot/kit/internal/pkg/redis"
)

// 配置结构体
type Config struct {
	Name     string             `mapstructure:"name,omitempty" ini:"name,omitempty" json:"name,omitempty" toml:"name,omitempty" xml:"name,omitempty" yaml:"name,omitempty" validate:"required"`                            // 应用名称
	Mode     string             `mapstructure:"mode,omitempty" ini:"mode,omitempty" json:"mode,omitempty" toml:"mode,omitempty" xml:"mode,omitempty" yaml:"mode,omitempty" validate:"required,oneof=develop test release"` // 模式[develop,test,release]
	File     *File              `mapstructure:"file,omitempty" ini:"file,omitempty" json:"file,omitempty" toml:"file,omitempty" xml:"file,omitempty" yaml:"file,omitempty" validate:"required"`                            // 配置文件信息
	Log      *log.Logger        `mapstructure:"log,omitempty" ini:"log,omitempty" json:"log,omitempty" toml:"log,omitempty" xml:"log,omitempty" yaml:"log,omitempty" validate:"required"`                                  // 日志配置信息
	Kafka    *kafka.Kafka       `mapstructure:"kafka,omitempty" ini:"kafka,omitempty" json:"kafka,omitempty" toml:"kafka,omitempty" xml:"kafka,omitempty" yaml:"kafka,omitempty" validate:"-"`                             // kafka
	Redis    *redis.Redis       `mapstructure:"redis,omitempty" ini:"redis,omitempty" json:"redis,omitempty" toml:"redis,omitempty" xml:"redis,omitempty" yaml:"redis,omitempty" validate:"-"`                             // redis
	Influxdb *influxdb.Influxdb `mapstructure:"influxdb,omitempty" ini:"influxdb,omitempty" json:"influxdb,omitempty" toml:"influxdb,omitempty" xml:"influxdb,omitempty" yaml:"influxdb,omitempty" validate:"-"`           // influxdb
	Mysql    *mysql.MySQL       `mapstructure:"mysql,omitempty" ini:"mysql,omitempty" json:"mysql,omitempty" toml:"mysql,omitempty" xml:"mysql,omitempty" yaml:"mysql,omitempty" validate:"-"`                             // mysql
}
