package config

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitee.com/waiot/kit/internal/pkg/log"
	"gitee.com/waiot/kit/internal/pkg/mysql"
	"gitee.com/waiot/kit/internal/pkg/redis"
	"gitee.com/waiot/kit/internal/pkg/util"
	toml "github.com/pelletier/go-toml/v2"
	"gopkg.in/ini.v1"
	"gopkg.in/yaml.v3"
)

type File struct {
	Path     string `mapstructure:"path" ini:"path,omitempty" json:"path,omitempty" toml:"path,omitempty" xml:"path,omitempty" yaml:"path,omitempty" validate:"required,dir"`                                       // 配置文件目录(绝对目录)
	Name     string `mapstructure:"name" ini:"name,omitempty" json:"name,omitempty" toml:"name,omitempty" xml:"name,omitempty" yaml:"name,omitempty" validate:"required"`                                           // 配置文件名称
	Type     string `mapstructure:"type" ini:"type,omitempty" json:"type,omitempty" toml:"type,omitempty" xml:"type,omitempty" yaml:"type,omitempty" validate:"required,oneof=ini json toml xml yml yaml makefile"` // 配置文件类型
	fullPath string // 配置文件绝对全路径
}

// 校验待生成配置文件信息
func (cf *File) verify() error {
	fpath, err := filepath.Abs(cf.Path)
	if err != nil {
		return err
	}
	if len(strings.TrimSpace(cf.Name)) == 0 {
		return errors.New("file name is not allowed to be empty")
	}
	if _, err = os.Stat(fpath); os.IsNotExist(err) {
		if merr := os.MkdirAll(fpath, os.ModePerm); merr != nil {
			return merr
		}
	}
	if strings.EqualFold(cf.Type, "makefile") {
		cf.fullPath = filepath.Join(fpath, cf.Name)
	} else {
		cf.fullPath = filepath.Join(fpath, strings.Join([]string{cf.Name, strings.ToLower(cf.Type)}, "."))
	}
	_, err = os.Stat(cf.fullPath)
	if err != nil && !os.IsNotExist(err) {
		return err
	}
	return util.ValidateStruct(cf, "zh", true)
}

// 生成文件
func (cf *File) Generate(name string) error {
	if err := cf.verify(); err != nil {
		return err
	}
	file, err := os.Create(cf.fullPath)
	if err != nil {
		return err
	}
	defer file.Close()
	config := &Config{
		Name: name,
		Mode: "develop",
		Log: &log.Logger{
			Record:    "logrus",
			Develop:   true,
			Namespace: "app",
			Format:    "text",
			Output: &log.OutPut{
				Console:  true,
				File:     true,
				Kafka:    false,
				Redis:    false,
				Influxdb: false,
				Mysql:    false,
			},
			// Log: &log.Log{},
			Logrus: &log.Logrus{
				Level: "trace",
			},
			Zap: &log.Zap{
				Level: "debug",
			},
			File: &log.File{
				Path:   "./logs",
				Prefix: "app",
				Suffix: &log.Suffix{
					Trace:  "trace",
					Debug:  "debug",
					Info:   "info",
					Warn:   "warn",
					Error:  "error",
					Dpanic: "dpanic",
					Panic:  "panic",
					Fatal:  "fatal",
				},
				Max: &log.Max{
					Size:    10,
					Backups: 30,
					Age:     30,
				},
			},
		},
		// Kafka: &kafka.Kafka{},
		Redis: &redis.Redis{
			Network:      "tcp",            // 连接网络类型，默认为tcp
			Address:      "127.0.0.1:6379", // 访问地址：ip+port
			UserName:     "username",       // 访问用户,如果未禁用default用户，可以为空或者default
			Password:     "password",       // 访问密码
			Database:     0,                // 使用数据库，默认为0
			PoolFIFO:     false,            // 连接池类型，true：FIFO（先入先出）；false：LIFO（后入先出，默认），相比之下FIFO具有更大的开销
			PoolSize:     10,               // 最大连接数，根据 runtime.GOMAXPROCS的cpu数，默认为每个可用CPU有10个连接
			PoolTimeout:  3 * time.Second,  // 连接池超时时间，默认为ReadTimeout+1秒
			MaxRetries:   3,                // 最大尝试次数，默认为3
			MinIdleConns: 5,                // 最小空闲连接
			MaxConnAge:   5 * time.Second,  // 连接最大生命周期，默认不关闭
			DialTimeout:  5 * time.Second,  // 连接最大超时时间，默认5秒
			ReadTimeout:  3 * time.Second,  // 读取超时时间，默认3秒
			WriteTimeout: 3 * time.Second,  // 写超时时间，默认与读超时相同
			IdleTimeout:  3 * time.Minute,  // 空闲连接超时时间，应该小于redis服务器的超时时间，默认为5分钟。 -1禁用空闲超时检查
		},
		// Influxdb: &influxdb.Influxdb{},
		Mysql: &mysql.MySQL{
			Driver:          "mysql",
			UserName:        "username",
			Password:        "password",
			Protocol:        "tcp",
			Address:         "127.0.0.1:3306",
			Database:        "database",
			ConnMaxLifeTime: 5 * time.Minute,
			ConnMaxIdleTime: time.Minute,
			MaxOpenConns:    10,
			MaxIdleConns:    10,
		},
	}
	switch {
	case strings.EqualFold(cf.Type, "ini"):
		return newIni(file, config)
	case strings.EqualFold(cf.Type, "json"):
		return newJson(file, config)
	case strings.EqualFold(cf.Type, "makefile"):
		return newMakefile(file)
	case strings.EqualFold(cf.Type, "toml"):
		return newToml(file, config)
	case strings.EqualFold(cf.Type, "xml"):
		return newXml(file, config)
	case strings.EqualFold(cf.Type, "yaml"), strings.EqualFold(cf.Type, "yml"):
		return newYaml(file, config)
	default:
		return fmt.Errorf("%s format is not supported", cf.Type)
	}
}
func newIni(file *os.File, config *Config) error {
	cfg := ini.Empty()
	if err := ini.ReflectFrom(cfg, config); err != nil {
		return err
	}
	// return cfg.SaveToIndent(file.Name(), "\t")
	return cfg.SaveTo(file.Name())
}
func newJson(file *os.File, config *Config) error {
	data, err := json.Marshal(config)
	if err != nil {
		return err
	}
	_, err = file.Write(data)
	return err
}
func newMakefile(file *os.File) error {
	data := `.PHONY: build and run
# 编译传入参数: 应用名称,默认 kit
NAME=kit
# 编译传入参数: 公司名称信息,默认 WAIOT Technology Co., Ltd.
COMPANY=WAIOT Technology Co., Ltd.
# 编译传入参数: 主版本号,默认为是1
MAJOR_VERSION=1
# 编译传入参数: 次版本号,默认为0
MINOR_VERSION=0
# 编译传入参数: 修正版本号,默认为0
REVISION=0
# 编译传入参数: 编译版本号,建议[年月日时分秒]
COMPILE_VERSION=$(shell date +'%Y%m%d%H%M%S')
# 编译传入参数: 英文常见号,默认为Alpha[内部测试]
COMMON_NUMBER=Alpha
# 编译传入参数: 著作权起始年份,默认为当前年份
YEAR_OF_COPYRIGHT=2019
# 编译传入参数: Golang版本
GO_VERSION=$(shell go version)
# 编译传入参数: 编译时间
BUILD_TIME=$(shell date +'%FT%T%z')
# 编译传入参数: 编译使用分支
BRANCH=$(shell git rev-parse --abbrev-ref HEAD)
# 编译传入参数: 编译分支最近一次提交信息
COMMIT=$(shell git log --pretty=format:'%cd %an %H' -n 1)
# 编译传入参数:  编译时Golang 的版本
GO_VERSION=$(shell go version)
# 编译传入参数: 描述信息
DESCRIPTION=World Wide Web and Artificial Intelligence and Internet of Things
# Golang 参数
# 是否支持 CGO 交叉汇编, 值只能是 0, 1
CGO_ENABLED=0
# 启用go mod
GO111MODULE=on
# 操作系统
GOOS_LINUX=linux
GOOS_DARWIN=darwin
GOOS_WINDOWS=windows
# CPU 架构
GOARCH_AMD64=amd64
GOARCH_386=386
GOARCH_ARM64=arm64
GOARCH_ARMHF=armhf
# GOARM: 只有 GOARCH 是 arm64 才有效, 表示 arm 的版本, 只能是 5, 6, 7 其中之一
GOARM_ARM64_5=5
GOARM_ARM64_6=6
GOARM_ARM64_7=7
MAIN=./cmd/main.go
BIN_PATH=./bin

# 当前操作系统
CURRENT_OS=$(shell go env GOOS)
# 当前架构
CURRENT_ARCH=$(shell go env GOARCH)

PARAMETER="\
-X 'main.name=$(NAME)' \
-X 'main.company=$(COMPANY)' \
-X 'main.majorVersion=$(MAJOR_VERSION)' \
-X 'main.minorVersion=$(MINOR_VERSION)' \
-X 'main.revision=$(REVISION)' \
-X 'main.compileVersion=$(COMPILE_VERSION)' \
-X 'main.commonNumber=$(COMMON_NUMBER)' \
-X 'main.yearOfCopyright=$(YEAR_OF_COPYRIGHT)' \
-X 'main.goVersion=$(GO_VERSION)' \
-X 'main.buildTime=$(BUILD_TIME)' \
-X 'main.branch=$(BRANCH)' \
-X 'main.commit=$(COMMIT)' \
-X 'main.description=$(DESCRIPTION)' \
-extldflags '-static'"

SERVICE='[Unit]\
\nDescription=$(NAME)\
\nWants=network-online.target\
\nAfter=net.target\
\n\
\n[Service]\
\nUser=root\
\nGroup=root\
\nWorkingDirectory=/opt/waiot/$(NAME)\
\nExecStart=/opt/waiot/$(NAME)/$(NAME)\
\nRestart=always\
\nRestartSec=45\
\n\
\n[Install]\
\nWantedBy=multi-user.target'


clean:
	rm -rf $(BIN_PATH)

prepare:
	go get -u -v ./...
	go mod tidy
	go mod vendor

# 跨平台编译
build:
	make clean
	make prepare
	make build-linux
	make build-darwin
	make build-windows


# linux
build-linux:
	make prepare
	make build-linux-amd64
	make build-linux-386
	make build-linux-arm64-5
	make build-linux-arm64-6
	make build-linux-arm64-7
build-linux-amd64:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_LINUX) GOARCH=$(GOARCH_AMD64) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_LINUX)/$(GOARCH_AMD64)/$(NAME) $(MAIN)
build-linux-386:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_LINUX) GOARCH=$(GOARCH_386) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_LINUX)/$(GOARCH_386)/$(NAME) $(MAIN)
build-linux-arm64-5:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_LINUX) GOARCH=$(GOARCH_ARM64) GOARM=$(GOARM_ARM64_5) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_LINUX)/$(GOARCH_ARM64)/$(GOARM_ARM64_5)/$(NAME) $(MAIN)
build-linux-arm64-6:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_LINUX) GOARCH=$(GOARCH_ARM64) GOARM=$(GOARM_ARM64_6) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_LINUX)/$(GOARCH_ARM64)/$(GOARM_ARM64_6)/$(NAME) $(MAIN)
build-linux-arm64-7:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_LINUX) GOARCH=$(GOARCH_ARM64) GOARM=$(GOARM_ARM64_7) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_LINUX)/$(GOARCH_ARM64)/$(GOARM_ARM64_7)/$(NAME) $(MAIN)

# darwin
build-darwin:
	make prepare
	make build-darwin-amd64
	make build-darwin-arm
	make build-darwin-arm64-5
	make build-darwin-arm64-6
	make build-darwin-arm64-7
build-darwin-amd64:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_DARWIN) GOARCH=$(GOARCH_AMD64) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_DARWIN)/$(GOARCH_AMD64)/$(NAME) $(MAIN)
build-darwin-arm:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_DARWIN) GOARCH=$(GOARCH_ARM64) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_DARWIN)/$(GOARCH_ARM64)/$(NAME) $(MAIN)
build-darwin-arm64-5:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_DARWIN) GOARCH=$(GOARCH_ARM64) GOARM=$(GOARM_ARM64_5) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_DARWIN)/$(GOARCH_ARM64)/$(GOARM_ARM64_5)/$(NAME) $(MAIN)
build-darwin-arm64-6:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_DARWIN) GOARCH=$(GOARCH_ARM64) GOARM=$(GOARM_ARM64_6) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_DARWIN)/$(GOARCH_ARM64)/$(GOARM_ARM64_6)/$(NAME) $(MAIN)
build-darwin-arm64-7:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_DARWIN) GOARCH=$(GOARCH_ARM64) GOARM=$(GOARM_ARM64_7) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_DARWIN)/$(GOARCH_ARM64)/$(GOARM_ARM64_7)/$(NAME) $(MAIN)

# windows
build-windows:
	make prepare
	make build-windows-amd64
	make build-windows-386
build-windows-amd64:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_WINDOWS) GOARCH=$(GOARCH_AMD64) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_WINDOWS)/$(GOARCH_AMD64)/$(NAME).exe $(MAIN)
build-windows-386:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_WINDOWS) GOARCH=$(GOARCH_386) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_WINDOWS)/$(GOARCH_386)/$(NAME).exe $(MAIN)	
	`
	_, err := file.WriteString(data)
	return err
}
func newToml(file *os.File, config *Config) error {
	data, err := toml.Marshal(config)
	if err != nil {
		return err
	}
	_, err = file.Write(data)
	return err
}
func newXml(file *os.File, config *Config) error {
	data, err := xml.Marshal(config)
	if err != nil {
		return err
	}
	_, err = file.Write(data)
	return err
}
func newYaml(file *os.File, config *Config) error {
	data, err := yaml.Marshal(config)
	if err != nil {
		return err
	}
	_, err = file.Write(data)
	return err
}
