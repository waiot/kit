package redis

import "time"

type Redis struct {
	Network      string        `mapstructure:"network" ini:"network" json:"network" toml:"network" xml:"network" yaml:"network" validate:"-"`                                           // 连接网络类型，默认为tcp
	Address      string        `mapstructure:"address" ini:"address" json:"address" toml:"address" xml:"address" yaml:"address" validate:"-"`                                           // 访问地址：ip+port
	UserName     string        `mapstructure:"username" ini:"username" json:"username" toml:"username" xml:"username" yaml:"username" validate:"-"`                                     // 访问用户,如果未禁用default用户，可以为空或者default
	Password     string        `mapstructure:"password" ini:"password" json:"password" toml:"password" xml:"password" yaml:"password" validate:"-"`                                     // 访问密码
	Database     uint8         `mapstructure:"database" ini:"database" json:"database" toml:"database" xml:"database" yaml:"database" validate:"-"`                                     // 使用数据库，默认为0
	PoolFIFO     bool          `mapstructure:"pool_fifo" ini:"pool_fifo" json:"pool_fifo" toml:"pool_fifo" xml:"pool_fifo" yaml:"pool_fifo" validate:"-"`                               // 连接池类型，true：FIFO（先入先出）；false：LIFO（后入先出，默认），相比之下FIFO具有更大的开销
	PoolSize     uint8         `mapstructure:"pool_size" ini:"pool_size" json:"pool_size" toml:"pool_size" xml:"pool_size" yaml:"pool_size" validate:"-"`                               // 最大连接数，根据 runtime.GOMAXPROCS的cpu数，默认为每个可用CPU有10个连接
	PoolTimeout  time.Duration `mapstructure:"pool_timeout" ini:"pool_timeout" json:"pool_timeout" toml:"pool_timeout" xml:"pool_timeout" yaml:"pool_timeout" validate:"-"`             // 连接池超时时间，默认为ReadTimeout+1秒
	MaxRetries   uint8         `mapstructure:"max_retries" ini:"max_retries" json:"max_retries" toml:"max_retries" xml:"max_retries" yaml:"max_retries" validate:"-"`                   // 最大尝试次数，默认为3
	MinIdleConns uint          `mapstructure:"min_idle_conns" ini:"min_idle_conns" json:"min_idle_conns" toml:"min_idle_conns" xml:"min_idle_conns" yaml:"min_idle_conns" validate:"-"` // 最小空闲连接
	MaxConnAge   time.Duration `mapstructure:"max_conn_age" ini:"max_conn_age" json:"max_conn_age" toml:"max_conn_age" xml:"max_conn_age" yaml:"max_conn_age" validate:"-"`             // 连接最大生命周期，默认不关闭
	DialTimeout  time.Duration `mapstructure:"dial_timeout" ini:"dial_timeout" json:"dial_timeout" toml:"dial_timeout" xml:"dial_timeout" yaml:"dial_timeout" validate:"-"`             // 连接最大超时时间，默认5秒
	ReadTimeout  time.Duration `mapstructure:"read_timeout" ini:"read_timeout" json:"read_timeout" toml:"read_timeout" xml:"read_timeout" yaml:"read_timeout" validate:"-"`             // 读取超时时间，默认3秒
	WriteTimeout time.Duration `mapstructure:"write_timeout" ini:"write_timeout" json:"write_timeout" toml:"write_timeout" xml:"write_timeout" yaml:"write_timeout" validate:"-"`       // 写超时时间，默认与读超时相同
	IdleTimeout  time.Duration `mapstructure:"idle_timeout" ini:"idle_timeout" json:"idle_timeout" toml:"idle_timeout" xml:"idle_timeout" yaml:"idle_timeout" validate:"-"`             // 空闲连接超时时间，应该小于redis服务器的超时时间，默认为5分钟。 -1禁用空闲超时检查
}
