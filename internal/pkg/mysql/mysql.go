package mysql

import (
	"fmt"
	"net"
	"strings"
	"time"

	"gitee.com/waiot/kit/internal/pkg/util"
	"github.com/shopspring/decimal"
)

type MySQL struct {
	Driver          string        `mapstructure:"driver" ini:"driver" json:"driver" toml:"driver" xml:"driver" yaml:"driver" validate:"required,oneof=mysql"`
	UserName        string        `mapstructure:"username" ini:"username" json:"username" toml:"username" xml:"username" yaml:"username" validate:"required"`
	Password        string        `mapstructure:"password" ini:"password" json:"password" toml:"password" xml:"password" yaml:"password" validate:"-"`
	Protocol        string        `mapstructure:"protocol" ini:"protocol" json:"protocol" toml:"protocol" xml:"protocol" yaml:"protocol" validate:"omitempty,oneof=tcp tcp4 tcp6 udp udp4 udp6 ip ip4 ip6 unix unixgram unixpacket"`
	Address         string        `mapstructure:"address" ini:"address" json:"address" toml:"address" xml:"address" yaml:"address" validate:"-"`
	Database        string        `mapstructure:"database" ini:"database" json:"database" toml:"database" xml:"database" yaml:"database" validate:"-"`
	Parameters      *Parameters   `mapstructure:"parameters,omitempty" ini:"mysql.parameters,omitempty" json:"parameters,omitempty" toml:"parameters,omitempty" xml:"parameters,omitempty" yaml:"parameters,omitempty" validate:"-"`
	ConnMaxLifeTime time.Duration `mapstructure:"conn_max_life_time" ini:"conn_max_life_time" json:"conn_max_life_time" toml:"conn_max_life_time" xml:"conn_max_life_time" yaml:"conn_max_life_time" validate:"-"`
	ConnMaxIdleTime time.Duration `mapstructure:"conn_max_idle_time" ini:"conn_max_idle_time" json:"conn_max_idle_time" toml:"conn_max_idle_time" xml:"conn_max_idle_time" yaml:"conn_max_idle_time" validate:"ltfield=ConnMaxLifeTime"`
	MaxOpenConns    int           `mapstructure:"max_open_conns" ini:"max_open_conns" json:"max_open_conns" toml:"max_open_conns" xml:"max_open_conns" yaml:"max_open_conns" validate:"-"`
	MaxIdleConns    int           `mapstructure:"max_idle_conns" ini:"max_idle_conns" json:"max_idle_conns" toml:"max_idle_conns" xml:"max_idle_conns" yaml:"max_idle_conns" validate:"eqfield=MaxOpenConns"`
}
type Parameters struct {
	AllowAllFiles           bool          `mapstructure:"allow_allFiles" ini:"allow_allFiles" json:"allow_allFiles" toml:"allow_allFiles" xml:"allow_allFiles" yaml:"allow_allFiles" validate:""`
	AllowCleartextPasswords bool          `mapstructure:"allow_cleartext_passwords" ini:"allow_cleartext_passwords" json:"allow_cleartext_passwords" toml:"allow_cleartext_passwords" xml:"allow_cleartext_passwords" yaml:"allow_cleartext_passwords" validate:"-"`
	AllowNativePasswords    bool          `mapstructure:"allow_native_passwords" ini:"allow_native_passwords" json:"allow_native_passwords" toml:"allow_native_passwords" xml:"allow_native_passwords" yaml:"allow_native_passwords" validate:"-"`
	AllowOldPasswords       bool          `mapstructure:"allow_old_passwords" ini:"allow_old_passwords" json:"allow_old_passwords" toml:"allow_old_passwords" xml:"allow_old_passwords" yaml:"allow_old_passwords" validate:"-"`
	Charset                 string        `mapstructure:"charset" ini:"charset" json:"charset" toml:"charset" xml:"charset" yaml:"charset" validate:"-"` //charset=utf8mb4,utf8,不建议使用charset参数，因为它会向服务器发出额外的查询。除非需要备用行为，否则请使用排序规则collection。
	CheckConnLiveness       bool          `mapstructure:"check_conn_liveness" ini:"check_conn_liveness" json:"check_conn_liveness" toml:"check_conn_liveness" xml:"check_conn_liveness" yaml:"check_conn_liveness" validate:"-"`
	Collation               string        `mapstructure:"collation" ini:"collation" json:"collation" toml:"collation" xml:"collation" yaml:"collation" validate:"-"` //utf8mb4_general_ci ,utf8_general_ci
	ClientFoundRows         bool          `mapstructure:"client_found_rows" ini:"client_found_rows" json:"client_found_rows" toml:"client_found_rows" xml:"client_found_rows" yaml:"client_found_rows" validate:"-"`
	ColumnsWithAlias        bool          `mapstructure:"columns_with_alias" ini:"columns_with_alias" json:"columns_with_alias" toml:"columns_with_alias" xml:"columns_with_alias" yaml:"columns_with_alias" validate:"-"`
	InterpolateParams       bool          `mapstructure:"interpolate_params" ini:"interpolate_params" json:"interpolate_params" toml:"interpolate_params" xml:"interpolate_params" yaml:"interpolate_params" validate:"-"` // isdefault
	Loc                     string        `mapstructure:"loc" ini:"loc" json:"loc" toml:"loc" xml:"loc" yaml:"loc" validate:"timezone"`                                                                                    // when using parseTime=true 'Local" sets the system's location.
	MaxAllowedPacket        float64       `mapstructure:"max_allowed_packet" ini:"max_allowed_packet" json:"max_allowed_packet" toml:"max_allowed_packet" xml:"max_allowed_packet" yaml:"max_allowed_packet" validate:"-"` // 4194304 允许的最大数据包大小(以字节为单位)。缺省值为4mib，需要根据服务器的设置进行调整。maxAllowedPacket=0可以用来在每次连接时自动从服务器获取max_allowed_packet变量。
	MultiStatements         bool          `mapstructure:"multi_statements" ini:"multi_statements" json:"multi_statements" toml:"multi_statements" xml:"multi_statements" yaml:"multi_statements" validate:"-"`             // 一个查询中允许多个语句。虽然这允许批量查询，但也大大增加了SQL注入的风险。只返回第一个查询的结果，所有其他结果将被静默丢弃。当使用multiStatements时，?参数只能在第一条语句中使用。
	ParseTime               bool          `mapstructure:"parse_time" ini:"parse_time" json:"parse_time" toml:"parse_time" xml:"parse_time" yaml:"parse_time" validate:"-"`
	ReadTimeout             time.Duration `mapstructure:"read_timeout" ini:"read_timeout" json:"read_timeout" toml:"read_timeout" xml:"read_timeout" yaml:"read_timeout" validate:"-"` // I/O read timeout. The value must be a decimal number with a unit suffix ("ms", "s", "m", "h"), such as "30s", "0.5m" or "1m30s".
	RejectReadOnly          bool          `mapstructure:"reject_read_only" ini:"reject_read_only" json:"reject_read_only" toml:"reject_read_only" xml:"reject_read_only" yaml:"reject_read_only" validate:"-"`
	ServerPubKey            string        `mapstructure:"server_pub_key" ini:"server_pub_key" json:"server_pub_key" toml:"server_pub_key" xml:"server_pub_key" yaml:"server_pub_key" validate:"-"`
	Timeout                 time.Duration `mapstructure:"timeout" ini:"timeout" json:"timeout" toml:"timeout" xml:"timeout" yaml:"timeout" validate:"-"` //Timeout for establishing connections, aka dial timeout. The value must be a decimal number with a unit suffix ("ms", "s", "m", "h"), such as "30s", "0.5m" or "1m30s".
	Tls                     interface{}   `mapstructure:"tls" ini:"tls" json:"tls" toml:"tls" xml:"tls" yaml:"tls" validate:"-"`                         // Valid Values:   true, false, skip-verify, preferred, <name>
	WriteTimeout            time.Duration `mapstructure:"write_timeout" ini:"write_timeout" json:"write_timeout" toml:"write_timeout" xml:"write_timeout" yaml:"write_timeout" validate:"-"`
}

func (db *MySQL) DSN() (dsn string, err error) {
	if err := util.ValidateStruct(db, "zh", true); err != nil {
		return dsn, err
	}
	db.UserName = strings.TrimSpace(db.UserName)
	dsn = db.UserName
	db.Password = strings.TrimSpace(db.Password)
	if len(db.Password) > 0 {
		dsn = strings.Join([]string{dsn, db.Password}, ":")
	}
	dsn = strings.Join([]string{dsn, ""}, "@")
	db.Protocol = strings.TrimSpace(db.Protocol)
	if len(db.Protocol) > 0 {
		dsn = strings.Join([]string{dsn, db.Protocol}, "")
	}
	db.Address = strings.TrimSpace(db.Address)
	if len(db.Address) > 0 {
		host, port, err := net.SplitHostPort(db.Address)
		if err != nil {
			return dsn, err
		}
		ip := net.ParseIP(host)
		if ip.To16() != nil || ip.To4() != nil {
			dsn = strings.Join([]string{dsn, "("}, "")
			if strings.ContainsAny(ip.String(), ":") {
				dsn = strings.Join([]string{dsn, ip.String()}, "[")
				dsn = strings.Join([]string{dsn, port}, ":")
				dsn = strings.Join([]string{dsn, "]"}, "")
			} else {
				dsn = strings.Join([]string{dsn, ip.String()}, "")
				dsn = strings.Join([]string{dsn, port}, ":")
			}
			dsn = strings.Join([]string{dsn, ")"}, "")
		}
	}
	dsn = strings.Join([]string{dsn, "/"}, "")
	db.Database = strings.TrimSpace(db.Database)
	if len(db.Database) > 0 {
		dsn = strings.Join([]string{dsn, db.Database}, "")
	}
	parameter := ""
	if db.Parameters != nil {
		if db.Parameters.AllowAllFiles {
			allowAllFiles := strings.Join([]string{"allowAllFiles", "true"}, "=")
			parameter = strings.Join([]string{parameter, allowAllFiles}, "&")
		}
		if db.Parameters.AllowCleartextPasswords {
			allowCleartextPasswords := strings.Join([]string{"allowCleartextPasswords", "true"}, "=")
			parameter = strings.Join([]string{parameter, allowCleartextPasswords}, "&")
		}
		if db.Parameters.AllowNativePasswords {
			allowNativePasswords := strings.Join([]string{"allowNativePasswords", "true"}, "=")
			parameter = strings.Join([]string{parameter, allowNativePasswords}, "&")
		}
		if db.Parameters.AllowOldPasswords {
			allowOldPasswords := strings.Join([]string{"allowOldPasswords", "true"}, "=")
			parameter = strings.Join([]string{parameter, allowOldPasswords}, "&")
		}
		db.Parameters.Charset = strings.TrimSpace(db.Parameters.Charset)
		if len(db.Parameters.Charset) > 0 {
			charset := strings.Join([]string{"charset", db.Parameters.Charset}, "=")
			parameter = strings.Join([]string{parameter, charset}, "&")
		}
		if db.Parameters.CheckConnLiveness {
			checkConnLiveness := strings.Join([]string{"checkConnLiveness", "true"}, "=")
			parameter = strings.Join([]string{parameter, checkConnLiveness}, "&")
		}
		db.Parameters.Collation = strings.TrimSpace(db.Parameters.Collation)
		if len(db.Parameters.Collation) > 0 {
			collation := strings.Join([]string{"collation", db.Parameters.Collation}, "=")
			parameter = strings.Join([]string{parameter, collation}, "&")
		}
		if db.Parameters.ClientFoundRows {
			clientFoundRows := strings.Join([]string{"clientFoundRows", "true"}, "=")
			parameter = strings.Join([]string{parameter, clientFoundRows}, "&")
		}
		if db.Parameters.ColumnsWithAlias {
			columnsWithAlias := strings.Join([]string{"columnsWithAlias", "true"}, "=")
			parameter = strings.Join([]string{parameter, columnsWithAlias}, "&")
		}
		if db.Parameters.InterpolateParams {
			interpolateParams := strings.Join([]string{"interpolateParams", "true"}, "=")
			parameter = strings.Join([]string{parameter, interpolateParams}, "&")
		}
		db.Parameters.Loc = strings.TrimSpace(db.Parameters.Loc)
		if len(db.Parameters.Loc) > 0 {
			loc := strings.Join([]string{"loc", db.Parameters.Loc}, "=")
			parameter = strings.Join([]string{parameter, loc}, "&")
		}
		if db.Parameters.MaxAllowedPacket > 0 {
			maxAllowedPacket := strings.Join([]string{"maxAllowedPacket", decimal.NewFromFloat(db.Parameters.MaxAllowedPacket).String()}, "=")
			parameter = strings.Join([]string{parameter, maxAllowedPacket}, "&")
		}
		if db.Parameters.MultiStatements {
			multiStatements := strings.Join([]string{"multiStatements", "true"}, "=")
			parameter = strings.Join([]string{parameter, multiStatements}, "&")
		}
		if db.Parameters.ParseTime {
			parseTime := strings.Join([]string{"parseTime", "true"}, "=")
			parameter = strings.Join([]string{parameter, parseTime}, "&")
		}
		if db.Parameters.ReadTimeout > 0 {
			readTimeout := strings.Join([]string{"readTimeout", db.Parameters.ReadTimeout.String()}, "=")
			parameter = strings.Join([]string{parameter, readTimeout}, "&")
		}
		if db.Parameters.RejectReadOnly {
			rejectReadOnly := strings.Join([]string{"rejectReadOnly", "true"}, "=")
			parameter = strings.Join([]string{parameter, rejectReadOnly}, "&")
		}
		db.Parameters.ServerPubKey = strings.TrimSpace(db.Parameters.ServerPubKey)
		if len(db.Parameters.ServerPubKey) > 0 {
			serverPubKey := strings.Join([]string{"serverPubKey", db.Parameters.ServerPubKey}, "=")
			parameter = strings.Join([]string{parameter, serverPubKey}, "&")
		}
		if db.Parameters.Timeout > 0 {
			timeout := strings.Join([]string{"timeout", db.Parameters.Timeout.String()}, "=")
			parameter = strings.Join([]string{parameter, timeout}, "&")
		}

		if !util.ValidateIsNil(db.Parameters.Tls) {
			tls := strings.Join([]string{"tls", fmt.Sprintf("%v", db.Parameters.Tls)}, "=")
			parameter = strings.Join([]string{parameter, tls}, "&")
		}
		if db.Parameters.WriteTimeout > 0 {
			writeTimeout := strings.Join([]string{"writeTimeout", db.Parameters.WriteTimeout.String()}, "=")
			parameter = strings.Join([]string{parameter, writeTimeout}, "&")
		}

	}
	parameter = strings.TrimSpace(parameter)
	if len(parameter) > 0 {
		parameter = strings.TrimPrefix(parameter, "&")
		parameter = strings.TrimSuffix(parameter, "&")
		dsn = strings.Join([]string{dsn, parameter}, "?")
	}
	return dsn, nil
}
