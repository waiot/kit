package main

import (
	"context"
	"math/rand"
	"time"

	"gitee.com/waiot/kit/cmd/root"
)

var (
	name            string // 编译传入参数: 应用名称,默认 kit
	company         string // 编译传入参数: 公司名称信息,默认 WAIOT Technology Co., Ltd.
	majorVersion    string // 编译传入参数: 主版本号,默认为是1
	minorVersion    string // 编译传入参数: 次版本号,默认为0
	revision        string // 编译传入参数: 修正版本号,默认为0
	compileVersion  string // 编译传入参数: 编译版本号,[build-编译版本号("build-"标记可选)],建议[年月日时分秒]
	commonNumber    string // 编译传入参数: 英文常见号,默认为Alpha[内部测试]
	yearOfCopyright string // 编译传入参数: 著作权起始年份,默认为当前年份
	goVersion       string // 编译传入参数: Golang版本
	buildTime       string // 编译传入参数: 编译时间
	branch          string // 编译传入参数: 编译使用分支
	commit          string // 编译传入参数: 编译分支最近一次提交信息
	description     string // 编译传入参数: 描述信息
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	root.NewBuilder(name, company, majorVersion, minorVersion, revision, compileVersion, commonNumber, yearOfCopyright, goVersion, buildTime, branch, commit, description)
	root.Execute(ctx)
}
