package root

import (
	"strconv"
	"strings"
	"time"
)

var builder *Build

// 版本信息:
// 格式:  主版本号.次版本[.修正版本号 [build-编译版本号("build-"标记可选)] 英文常见号】
// 英文常见号一般有GA、SNAP-SHOT、RELEASE、ALPHA、BETA、RC等版本号。
// Alpha: 内部测试版; Beta: 外部测试版; Build: 修正版; Corporation/Enterprise: 企业版; Delux: 豪华版; DEMO: 演示版,有功能限制; Free: 免费版; Full: 完全版; Final: 正式版; Pro(professional): 专业版; Plus: 加强版; Retail: 零售版; Release: 发行版,有时间限制; Shareware: 共享版，虽然不会要求注册但是一般也有功能限制; SR: 修正版; Trial: 试用版（一般有时间或者功能限制）
type Build struct {
	Name            string `mapstructure:"name" json:"name" yaml:"name" toml:"name" validate:""`                                                     // 编译传入参数: 应用名称,默认 kit
	Company         string `mapstructure:"company" json:"company" yaml:"company" toml:"company" validate:""`                                         // 编译传入参数: 公司名称信息,默认 WAIOT Technology Co., Ltd.
	MajorVersion    string `mapstructure:"major_version" json:"major_version" yaml:"major_version" toml:"major_version" validate:""`                 // 编译传入参数: 主版本号,默认为是1
	MinorVersion    string `mapstructure:"minor_version" json:"minor_version" yaml:"minor_version" toml:"minor_version" validate:""`                 // 编译传入参数: 次版本号,默认为0
	Revision        string `mapstructure:"revision" json:"revision" yaml:"revision" toml:"revision" validate:""`                                     // 编译传入参数: 修正版本号,默认为0
	CompileVersion  string `mapstructure:"compile_version" json:"compile_version" yaml:"compile_version" toml:"compile_version" validate:""`         // 编译传入参数: 编译版本号,[build-编译版本号("build-"标记可选)],建议[年月日时分秒]
	CommonNumber    string `mapstructure:"common_number" json:"common_number" yaml:"common_number" toml:"common_number" validate:""`                 // 编译传入参数: 英文常见号,默认为Alpha[内部测试]
	YearOfCopyright string `mapstructure:"year_of_copyright" json:"year_of_copyright" yaml:"year_of_copyright" toml:"year_of_copyright" validate:""` // 编译传入参数: 著作权起始年份,默认为当前年份
	GoVersion       string `mapstructure:"go_version" json:"go_version" yaml:"go_version" toml:"go_version" validate:""`                             // 编译传入参数: Golang版本
	BuildTime       string `mapstructure:"build_time" json:"build_time" yaml:"build_time" toml:"build_time" validate:""`                             // 编译传入参数: 编译时间
	Branch          string `mapstructure:"branch" json:"branch" yaml:"branch" toml:"branch" validate:""`                                             // 编译传入参数: 编译使用分支
	Commit          string `mapstructure:"commit" json:"commit" yaml:"commit" toml:"commit" validate:""`                                             // 编译传入参数: 编译分支最近一次提交信息
	Description     string `mapstructure:"description" json:"description" yaml:"description" toml:"description" validate:""`                         // 编译传入参数: 描述信息
	Copyright       string `mapstructure:"copyright" json:"copyright" yaml:"copyright" toml:"copyright" validate:""`                                 // 拼接结果: 版权信息: © 著作权起始年份-当前年份 Copyright by 公司名称信息 All rights reserved
	Version         string `mapstructure:"version" json:"version" yaml:"version" toml:"version" validate:""`                                         // 拼接结果: 应用版本: 主版本号.次版本[.修正版本号 [build-编译版本号("build-"标记可选)] 英文常见号]
	Summary         string `mapstructure:"summary" json:"summary" yaml:"summary" toml:"summary" validate:""`                                         // 拼接结果: 摘要信息: 版权信息 + 应用版本
	Information     string `mapstructure:"information" json:"information" yaml:"information" toml:"information" validate:""`                         // 拼接结果: 详细信息
}

func NewBuilder(name, company, majorVersion, minorVersion, revision, compileVersion, commonNumber,
	yearOfCopyright, goVersion, buildTime, branch, commit, description string) {
	name = strings.TrimSpace(name)
	company = strings.TrimSpace(company)
	majorVersion = strings.TrimSpace(majorVersion)
	minorVersion = strings.TrimSpace(minorVersion)
	revision = strings.TrimSpace(revision)
	compileVersion = strings.TrimSpace(compileVersion)
	commonNumber = strings.TrimSpace(commonNumber)
	yearOfCopyright = strings.TrimSpace(yearOfCopyright)
	goVersion = strings.TrimSpace(goVersion)
	buildTime = strings.TrimSpace(buildTime)
	branch = strings.TrimSpace(branch)
	commit = strings.TrimSpace(commit)
	description = strings.TrimSpace(description)
	copyright := ""
	version := ""
	summary := ""
	information := ""
	// 编译传入参数: 应用名称,默认 kit
	if len(name) == 0 {
		name = "kit"
	}
	// 编译传入参数: 公司名称信息,默认 WAIOT Technology Co., Ltd.
	if len(company) == 0 {
		company = "WAIOT Technology Co., Ltd."
	}
	// 编译传入参数: 主版本号,默认为是1
	if len(majorVersion) == 0 {
		majorVersion = "1"
	}
	// 编译传入参数: 次版本号,默认为0
	if len(minorVersion) == 0 {
		minorVersion = "0"
	}
	// 编译传入参数: 修正版本号,默认为0
	if len(revision) == 0 {
		revision = "0"
	}
	// 编译传入参数: 编译版本号,[build-编译版本号("build-"标记可选)],建议[年月日时分秒]
	if len(compileVersion) > 0 {
		compileVersion = strings.Join([]string{"build", compileVersion}, "-")
	}
	// 编译传入参数: 英文常见号,默认为Alpha[内部测试]
	if len(commonNumber) == 0 {
		commonNumber = "Alpha"
	}
	// 编译传入参数: 著作权起始年份,默认为当前年份
	if len(yearOfCopyright) == 0 {
		yearOfCopyright = strings.Join([]string{strconv.Itoa(time.Now().Year()), ""}, "")
	}
	// 拼接结果: 版权信息: © 20022-当前年份 Copyright by 公司名称信息 All rights reserved
	copyright = strings.Join([]string{"©", yearOfCopyright}, " ")
	copyright = strings.Join([]string{copyright, strconv.Itoa(time.Now().Year())}, "-")
	copyright = strings.Join([]string{copyright, "Copyright", "by", company, "All", "rights", "reserved"}, " ")
	// 拼接结果: 应用版本: 主版本号.次版本[.修正版本号 [build-编译版本号("build-"标记可选)] 英文常见号]
	if len(compileVersion) == 0 {
		version = strings.Join([]string{majorVersion, minorVersion, revision}, ".")
		version = strings.Join([]string{version, commonNumber}, " ")
	} else {
		version = strings.Join([]string{majorVersion, minorVersion, revision}, ".")
		version = strings.Join([]string{version, compileVersion, commonNumber}, " ")
	}
	// 拼接结果: 摘要信息: 版权信息 + 应用版本
	summary = strings.Join([]string{copyright, version}, " ")
	summary = strings.Join([]string{summary, "\r\n"}, "")
	// 拼接结果: 详细信息
	// 1.编译传入参数: 应用名称,默认 Application
	information = strings.Join([]string{"Name:", " ", name, "\r\n"}, "")
	// 2.拼接结果: 应用版本: 主版本号.次版本[.修正版本号 [build-编译版本号("build-"标记可选)] 英文常见号]
	information = strings.Join([]string{information, "Version:", " ", version, "\r\n"}, "")
	// 3.拼接结果: 版权信息: © 20022-当前年份 Copyright by 公司名称信息 All rights reserved
	information = strings.Join([]string{information, "Copyright:", " ", copyright, "\r\n"}, "")
	// 4.编译传入参数: Golang版本
	if len(buildTime) > 0 {
		information = strings.Join([]string{information, "Golang-Version:", " ", goVersion, "\r\n"}, "")
	}
	// 5.编译传入参数: 编译时间
	if len(buildTime) > 0 {
		information = strings.Join([]string{information, "Build-Time:", " ", buildTime, "\r\n"}, "")
	}
	// 6.编译传入参数: 编译使用分支
	if len(branch) > 0 {
		information = strings.Join([]string{information, "Branch:", " ", branch, "\r\n"}, "")
	}
	// 7.编译传入参数: 编译分支最近一次提交信息
	if len(commit) > 0 {
		information = strings.Join([]string{information, "Commit:", " ", commit, "\r\n"}, "")
	}
	// 8.编译传入参数: 描述信息
	if len(description) > 0 {
		information = strings.Join([]string{information, "Description:", " ", description, "\r\n"}, "")
	}
	builder = &Build{
		Name:            name,
		Company:         company,
		MajorVersion:    majorVersion,
		MinorVersion:    minorVersion,
		Revision:        revision,
		CompileVersion:  compileVersion,
		CommonNumber:    commonNumber,
		YearOfCopyright: yearOfCopyright,
		GoVersion:       goVersion,
		BuildTime:       buildTime,
		Branch:          branch,
		Commit:          commit,
		Description:     description,
		Copyright:       copyright,
		Version:         version,
		Summary:         summary,
		Information:     information,
	}
}
