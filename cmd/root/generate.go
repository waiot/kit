package root

import (
	"github.com/spf13/cobra"
)

var generateCmd = &cobra.Command{
	Use:     "generate",
	Aliases: []string{"g"},
	Short:   "Generate templates",
	Long:    "Generate templates",
}

func init() {
	generateCmd.AddCommand(configCmd, makefileCmd)
}
