package root

import (
	"os"
	"strings"

	"github.com/spf13/cobra"
)

var informationCmd = &cobra.Command{
	Use:     "information",
	Aliases: []string{"info", "i"},
	Short:   "Show Application detail information",
	Long:    "Show Application detail information",
	Run: func(cmd *cobra.Command, args []string) {
		cobra.WriteStringAndCheck(os.Stdout, strings.Join([]string{builder.Information}, "\r\n"))
	},
}
