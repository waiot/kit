package root

import (
	"gitee.com/waiot/kit/internal/pkg/config"
	"github.com/spf13/cobra"
)

var configCmd = &cobra.Command{
	Use:     "config",
	Aliases: []string{"cfg", "c"},
	Short:   "Generate config templates",
	Long:    "Generate config templates",
	Run: func(cmd *cobra.Command, args []string) {
		types := []string{"ini", "json", "toml", "xml", "yaml", "yml"}
		for _, t := range types {
			cf := config.File{
				Path: path,
				Name: name,
				Type: t,
			}
			err = cf.Generate(name)
			cobra.CheckErr(err)
		}
	},
}

func init() {
	configCmd.Flags().StringVarP(&path, "path", "p", "./templates/configs", "generate config's file templates path.")
	configCmd.Flags().StringVarP(&name, "name", "n", "application", "generate config's file templates name.")
	configCmd.ParseFlags([]string{"path", "name"})
}
