package root

import (
	"os"
	"strings"

	"github.com/spf13/cobra"
	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

var versionCmd = &cobra.Command{
	Use:     "version",
	Aliases: []string{"ver", "v"},
	Short:   "Show Application version",
	Long:    "Show Application version",
	Run: func(cmd *cobra.Command, args []string) {
		cobra.WriteStringAndCheck(os.Stdout, strings.Join([]string{cases.Title(language.Und).String(builder.Name), "Version:", builder.Version, "\r\n"}, " "))
	},
}
