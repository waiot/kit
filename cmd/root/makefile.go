package root

import (
	"gitee.com/waiot/kit/internal/pkg/config"
	"github.com/spf13/cobra"
)

var makefileCmd = &cobra.Command{
	Use:     "makefile",
	Aliases: []string{"mk", "mf", "m"},
	Short:   "Generate Makefile templates",
	Long:    "Generate Makefile templates",
	Run: func(cmd *cobra.Command, args []string) {
		cf := config.File{
			Path: path,
			Name: "Makefile",
			Type: "makefile",
		}
		err = cf.Generate(name)
		cobra.CheckErr(err)
	},
}

func init() {
	makefileCmd.Flags().StringVarP(&path, "path", "p", "./templates/makefile", "generate makefile's file templates path.")
	makefileCmd.ParseFlags([]string{"path"})
}
