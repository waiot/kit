package root

import (
	"context"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

var (
	path string
	name string
	err  error
)
var rootCmd = &cobra.Command{
	Use:   "kit",
	Short: "kit",
	Long:  "waiot kit",
	PreRun: func(cmd *cobra.Command, args []string) {
		cobra.WriteStringAndCheck(os.Stdout, strings.Join([]string{builder.Information}, "\r\n"))
		// 	rcf, err := config.ReadInConfig(confFile)
		// 	cobra.CheckErr(err)
		// 	conf = rcf
		// 	conf.Log.Namespace = fmt.Sprintf("[%s %s %s]", build.Name, build.Branch, build.Version)
		// 	err = conf.Log.InitialLog()
		// 	cobra.CheckErr(err)
	},
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("++++++++++++++++++++++")
		// log.With(zap.Any("config", conf)).Debug("The configuration's information")
		// service.Startup(cmd.Context(), conf, build)
	},
	PostRun: func(cmd *cobra.Command, args []string) {
		// log := zap.L()
		// log.Sync()
		// service.Shutdown()
		// log.Info(fmt.Sprintf("The service %s is shutdown.", build.Name))
	},
}

func Execute(ctx context.Context) {
	err := rootCmd.ExecuteContext(ctx)
	cobra.CheckErr(err)
}

func init() {
	rootCmd.AddCommand(generateCmd, informationCmd, versionCmd)
}
