.PHONY: build and run
# 编译传入参数: 应用名称,默认 kit
NAME=kit
# 编译传入参数: 公司名称信息,默认 WAIOT Technology Co., Ltd.
COMPANY=WAIOT Technology Co., Ltd.
# 编译传入参数: 主版本号,默认为是1
MAJOR_VERSION=1
# 编译传入参数: 次版本号,默认为0
MINOR_VERSION=0
# 编译传入参数: 修正版本号,默认为0
REVISION=0
# 编译传入参数: 编译版本号,建议[年月日时分秒]
COMPILE_VERSION=$(shell date +'%Y%m%d%H%M%S')
# 编译传入参数: 英文常见号,默认为Alpha[内部测试]
COMMON_NUMBER=Alpha
# 编译传入参数: 著作权起始年份,默认为当前年份
YEAR_OF_COPYRIGHT=2019
# 编译传入参数: Golang版本
GO_VERSION=$(shell go version)
# 编译传入参数: 编译时间
BUILD_TIME=$(shell date +'%FT%T%z')
# 编译传入参数: 编译使用分支
BRANCH=$(shell git rev-parse --abbrev-ref HEAD)
# 编译传入参数: 编译分支最近一次提交信息
COMMIT=$(shell git log --pretty=format:'%cd %an %H' -n 1)
# 编译传入参数:  编译时Golang 的版本
GO_VERSION=$(shell go version)
# 编译传入参数: 描述信息
DESCRIPTION=World Wide Web and Artificial Intelligence and Internet of Things
# Golang 参数
# 是否支持 CGO 交叉汇编, 值只能是 0, 1
CGO_ENABLED=0
# 启用go mod
GO111MODULE=on
# 操作系统
GOOS_LINUX=linux
GOOS_DARWIN=darwin
GOOS_WINDOWS=windows
# CPU 架构
GOARCH_AMD64=amd64
GOARCH_386=386
GOARCH_ARM64=arm64
GOARCH_ARMHF=armhf
# GOARM: 只有 GOARCH 是 arm64 才有效, 表示 arm 的版本, 只能是 5, 6, 7 其中之一
GOARM_ARM64_5=5
GOARM_ARM64_6=6
GOARM_ARM64_7=7
MAIN=./cmd/main.go
BIN_PATH=./bin

# 当前操作系统
CURRENT_OS=$(shell go env GOOS)
# 当前架构
CURRENT_ARCH=$(shell go env GOARCH)

PARAMETER="\
-X 'main.name=$(NAME)' \
-X 'main.company=$(COMPANY)' \
-X 'main.majorVersion=$(MAJOR_VERSION)' \
-X 'main.minorVersion=$(MINOR_VERSION)' \
-X 'main.revision=$(REVISION)' \
-X 'main.compileVersion=$(COMPILE_VERSION)' \
-X 'main.commonNumber=$(COMMON_NUMBER)' \
-X 'main.yearOfCopyright=$(YEAR_OF_COPYRIGHT)' \
-X 'main.goVersion=$(GO_VERSION)' \
-X 'main.buildTime=$(BUILD_TIME)' \
-X 'main.branch=$(BRANCH)' \
-X 'main.commit=$(COMMIT)' \
-X 'main.description=$(DESCRIPTION)' \
-extldflags '-static'"

SERVICE='[Unit]\
\nDescription=$(NAME)\
\nWants=network-online.target\
\nAfter=net.target\
\n\
\n[Service]\
\nUser=root\
\nGroup=root\
\nWorkingDirectory=/opt/waiot/$(NAME)\
\nExecStart=/opt/waiot/$(NAME)/$(NAME)\
\nRestart=always\
\nRestartSec=45\
\n\
\n[Install]\
\nWantedBy=multi-user.target'


clean:
	rm -rf $(BIN_PATH)

prepare:
	go get -u -v ./...
	go mod tidy
	go mod vendor

# 跨平台编译
build:
	make clean
	make prepare
	make build-linux
	make build-darwin
	make build-windows


# linux
build-linux:
	make prepare
	make build-linux-amd64
	make build-linux-386
	make build-linux-arm64-5
	make build-linux-arm64-6
	make build-linux-arm64-7
build-linux-amd64:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_LINUX) GOARCH=$(GOARCH_AMD64) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_LINUX)/$(GOARCH_AMD64)/$(NAME) $(MAIN)
build-linux-386:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_LINUX) GOARCH=$(GOARCH_386) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_LINUX)/$(GOARCH_386)/$(NAME) $(MAIN)
build-linux-arm64-5:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_LINUX) GOARCH=$(GOARCH_ARM64) GOARM=$(GOARM_ARM64_5) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_LINUX)/$(GOARCH_ARM64)/$(GOARM_ARM64_5)/$(NAME) $(MAIN)
build-linux-arm64-6:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_LINUX) GOARCH=$(GOARCH_ARM64) GOARM=$(GOARM_ARM64_6) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_LINUX)/$(GOARCH_ARM64)/$(GOARM_ARM64_6)/$(NAME) $(MAIN)
build-linux-arm64-7:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_LINUX) GOARCH=$(GOARCH_ARM64) GOARM=$(GOARM_ARM64_7) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_LINUX)/$(GOARCH_ARM64)/$(GOARM_ARM64_7)/$(NAME) $(MAIN)

# darwin
build-darwin:
	make prepare
	make build-darwin-amd64
	make build-darwin-arm
	make build-darwin-arm64-5
	make build-darwin-arm64-6
	make build-darwin-arm64-7
build-darwin-amd64:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_DARWIN) GOARCH=$(GOARCH_AMD64) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_DARWIN)/$(GOARCH_AMD64)/$(NAME) $(MAIN)
build-darwin-arm:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_DARWIN) GOARCH=$(GOARCH_ARM64) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_DARWIN)/$(GOARCH_ARM64)/$(NAME) $(MAIN)
build-darwin-arm64-5:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_DARWIN) GOARCH=$(GOARCH_ARM64) GOARM=$(GOARM_ARM64_5) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_DARWIN)/$(GOARCH_ARM64)/$(GOARM_ARM64_5)/$(NAME) $(MAIN)
build-darwin-arm64-6:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_DARWIN) GOARCH=$(GOARCH_ARM64) GOARM=$(GOARM_ARM64_6) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_DARWIN)/$(GOARCH_ARM64)/$(GOARM_ARM64_6)/$(NAME) $(MAIN)
build-darwin-arm64-7:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_DARWIN) GOARCH=$(GOARCH_ARM64) GOARM=$(GOARM_ARM64_7) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_DARWIN)/$(GOARCH_ARM64)/$(GOARM_ARM64_7)/$(NAME) $(MAIN)

# windows
build-windows:
	make prepare
	make build-windows-amd64
	make build-windows-386
build-windows-amd64:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_WINDOWS) GOARCH=$(GOARCH_AMD64) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_WINDOWS)/$(GOARCH_AMD64)/$(NAME).exe $(MAIN)
build-windows-386:
	make prepare
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS_WINDOWS) GOARCH=$(GOARCH_386) GO111MODULE=$(GO111MODULE) \
	go build -a -ldflags $(PARAMETER) -mod vendor -o $(BIN_PATH)/$(GOOS_WINDOWS)/$(GOARCH_386)/$(NAME).exe $(MAIN)
